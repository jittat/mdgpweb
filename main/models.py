from django.db import models

class RequestToken(models.Model):
    requested_at = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def get_new_token():
        r = RequestToken()
        r.save()
        return r


