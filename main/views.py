import os

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.conf import settings

from models import RequestToken

def index(request):
    return render_to_response("main/index.html",
                              context_instance=RequestContext(request))

def save_tree_info(file_name,info):
    f = open(file_name,'w')
    print >> f, info
    f.close()

def find_mapping(request):
    token = RequestToken.get_new_token()
    token_id = token.id

    working_dir = settings.CLI_WORKING_DIR + str(token_id)
    
    os.mkdir(working_dir)

    stree_filename = working_dir + '/stree.in'
    gtree_pattern = working_dir + '/gtree.%d.in'
    gtree_names = working_dir + '/gtree.names'
    output_filename = working_dir + '/output.txt'

    save_tree_info(stree_filename,
                   request.POST['stree_rep'])

    gnames = []
    c = 1
    while ('gtree%d_rep' % c) in request.POST:
        save_tree_info((gtree_pattern % c),
                       request.POST['gtree%d_rep' % c])
        if request.POST['gtree%d_name' % c]!='':
            gnames.append(request.POST['gtree%d_name' % c])
        else:
            gnames.append('Gene tree %d' % c)
        c += 1

    save_tree_info(gtree_names,
                   '\n'.join(gnames))

    cmd = ('cd %s; python mgdp.py %s -gnames %s > %s' % 
           (settings.CLI_HOME,
            stree_filename + ' ' +
            ' '.join([(gtree_pattern % i) for i in range(1,c)]),
            gtree_names,
            output_filename))

    os.system(cmd)

    if os.path.exists(output_filename):
        output = open(output_filename).read()

    return render_to_response("main/mapping.html",
                              { 'output': output },
                              context_instance=RequestContext(request))


        
